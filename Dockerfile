FROM python:3.12.4-alpine

# Prepare environment & install dependencies
ENV PYTHONUNBUFFERED=1
RUN pip install --no-cache-dir build installer

# Prepare build folder
WORKDIR '/tmp/app'
COPY . '/tmp/app'

# Build & install
RUN python -m build
RUN python -m installer 'dist/'*'.whl'

# Clean up
WORKDIR /
RUN rm -rf '/tmp/app'

# Run
CMD [ "salvador-notifier", "-p", "80" ]
EXPOSE 80
HEALTHCHECK --interval=60s --timeout=10s CMD wget --post-data '{"healthcheck": true}' --header 'Content-Type: application/json' --tries=1 --spider http://127.0.0.1:80/ || exit 1
