__version__ = '1.1.2'

import argparse
import email.message
import http.server
import json
import os
import signal
import smtplib
import ssl
import sys
import time

# Modified HTTPServer class with customizations
class SalvadorNotifierServer(http.server.HTTPServer):
	# Add a prop for our custom handler to be called on incoming POST requests
	def serve_forever(self, post_handler):
		print('Starting server on port {}'.format(self.socket.getsockname()[1]))
		self.RequestHandlerClass.post_handler = post_handler
		http.server.HTTPServer.serve_forever(self)

	# Output messages when shutting down
	def server_close(self):
		print('Shutting down server')
		http.server.HTTPServer.server_close(self)
		print('Server shut down')

# Modified HTTPServer request handler class with customizations
class SalvadorNotifierRequestHandler(http.server.BaseHTTPRequestHandler):
	post_handler = None

	# Don't output any error messages for incoming requests
	def log_message(self, format, *args):
		pass

	# Call our handler with the data when we get a incoming POST request
	def do_POST(self):
		# Send an empty 200 response back to acknowledge the request
		self.send_response(200)
		self.end_headers()

		# Read the incoming data according to the Content-Length header
		length = self.headers['content-length']
		data = self.rfile.read(int(length))

		# Parse the JSON or use the fallback empty object
		if data and data != '':
			data = json.loads(data)
		else:
			data = {}

		# Send the data to our handler
		self.post_handler(data, self.headers)

	# Return a robots.txt file when we get a incoming GET request with a correct path
	def do_GET(self):
		# Check if we're working with a correct path
		if self.path == '/robots.txt':
			# Send a 200 response with the robots.txt file contents
			response = 'User-agent: *\nDisallow: /'

			self.send_response(200)
			self.send_header('Content-Type', 'text/plain')
			self.send_header('Content-Length', str(len(response)))
			self.end_headers()
			self.wfile.write(bytes(response, 'UTF-8'))

# Our main class, handles data validation & sending e-mails
class SalvadorNotifier:
	SALVADOR_CONTENT = 'created automatically by [salvador](https://gitlab.com/dpeukert/salvador).'
	GITLAB_TOKEN_HEADER_NAME = 'X-Gitlab-Token'

	# Validate the data and check if we want to send a e-mail
	def handle_data(self, raw_data, headers):
		token = os.environ.get('SALVADOR_NOTIFIER_TOKEN')

		if 'healthcheck' in raw_data:
			# Don't log, as we don't want to pollute the log with healthchecks
			return
		elif token is not None and (self.GITLAB_TOKEN_HEADER_NAME not in headers or headers[self.GITLAB_TOKEN_HEADER_NAME] != token):
			print('Correct token not provided, ignoring incoming request')
			return
		elif 'event_type' not in raw_data or (raw_data['event_type'] != 'issue' and raw_data['event_type'] != 'merge_request'):
			print('Event is not related to an issue or a merge request, ignoring incoming request')
			return
		elif 'project' not in raw_data or 'name' not in raw_data['project']:
			print('Project details not provided, ignoring incoming request')
			return
		elif 'object_attributes' not in raw_data or 'action' not in raw_data['object_attributes'] or 'description' not in raw_data['object_attributes'] or 'state' not in raw_data['object_attributes'] or 'title' not in raw_data['object_attributes'] or 'url' not in raw_data['object_attributes']:
			print('Object details not provided, ignoring incoming request')
			return
		elif raw_data['object_attributes']['state'] != 'opened':
			print('Object is not opened, ignoring incoming request')
			return
		elif self.SALVADOR_CONTENT not in raw_data['object_attributes']['description']:
			print('Object was not created by salvador, ignoring incoming request')
			return
		elif raw_data['object_attributes']['action'] != 'open' and raw_data['object_attributes']['action'] != 'reopen' and raw_data['object_attributes']['action'] != 'update':
			print('Object is not being created, reopened or updated, ignoring incoming request')
			return
		elif raw_data['object_attributes']['action'] == 'update' and ('changes' not in raw_data or ('title' not in raw_data['changes'] and 'description' not in raw_data['changes'])):
			print('Object is being updated, but the title and description did not change, ignoring incoming request')
			return

		# We passed validation, let's send an e-mail
		self.send_email(project_name=raw_data['project']['name'], object_name=raw_data['object_attributes']['title'], object_description=raw_data['object_attributes']['description'], object_link=raw_data['object_attributes']['url'])

	# Send an e-mail
	def send_email(self, project_name, object_name, object_description, object_link):
		# Prepare the message
		message = email.message.EmailMessage()
		message['Subject'] = '{} | {}'.format(project_name, object_name)
		message['From'] = os.environ.get('SALVADOR_NOTIFIER_FROM')
		message['To'] = os.environ.get('SALVADOR_NOTIFIER_TO')
		message.set_content('{}\n---\n{}'.format(object_description, object_link))

		# Send the message
		print('Sending an e-mail to "{}" regarding object "{}"'.format(message['To'], object_name))
		smtp = smtplib.SMTP(os.environ.get('SALVADOR_NOTIFIER_SMTP_HOST'), int(os.environ.get('SALVADOR_NOTIFIER_SMTP_PORT')))
		smtp.starttls(context=ssl.create_default_context())
		smtp.login(os.environ.get('SALVADOR_NOTIFIER_SMTP_USER'), os.environ.get('SALVADOR_NOTIFIER_SMTP_PASSWORD'))
		smtp.send_message(message)
		smtp.quit()
		print('E-mail sent')

def server():
	parser = argparse.ArgumentParser(
		formatter_class=argparse.RawTextHelpFormatter,
		description='\n'.join([
			'description:',
			'  {prog} is a utility that sends an email when salvador creates',
			'  a new issue/merge request or modifies an existing one by handling GitLab',
			'  webhooks.',
		]).format(prog=__name__.replace('_', '-')),
		prog=__name__.replace('_', '-')
	)

	parser.add_argument(
		'--version',
		action='version',
		version=__version__
	)

	parser.add_argument(
		'-p', '--port',
		type=int,
		default=80,
		help='set a port to listen on (80 if not specified)'
	)

	args = parser.parse_args()

	# Check if we have all the required env vars
	required_env_vars = [
		'SALVADOR_NOTIFIER_FROM',
		'SALVADOR_NOTIFIER_TO',
		'SALVADOR_NOTIFIER_SMTP_HOST',
		'SALVADOR_NOTIFIER_SMTP_PORT',
		'SALVADOR_NOTIFIER_SMTP_USER',
		'SALVADOR_NOTIFIER_SMTP_PASSWORD',
	]

	missing_env_vars = []

	for env_var in required_env_vars:
		if os.environ.get(env_var) is None:
			missing_env_vars.append(env_var)

	if len(missing_env_vars) > 0:
		print('Environment variables {} must be provided, aborting'.format(', '.join(missing_env_vars)))
		return

	# Initialize our main class
	salvador_notifier = SalvadorNotifier()

	# Keep trying to initialize our HTTP server until binding is succesful
	server_running = False

	while server_running is False:
		try:
			server = SalvadorNotifierServer(('', args.port), SalvadorNotifierRequestHandler)
		except OSError:
			print('Unable to bind, will retry in 0.1 seconds')
			time.sleep(0.1)
		else:
			server_running = True

	# Set up signal handling
	signal.signal(signal.SIGINT, lambda *args, **kwargs: server.server_close())
	signal.signal(signal.SIGTERM, lambda *args, **kwargs: server.server_close())

	# Run the HTTP server
	try:
		server.serve_forever(post_handler=salvador_notifier.handle_data)
	except KeyboardInterrupt:
		server.server_close()
