# salvador-notifier

This utility sends an email when [salvador](https://gitlab.com/dpeukert/salvador) creates a new issue/merge request or modifies an existing one by handling [GitLab webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html).

## How do I install this?

- fulfill the following dependencies
  - `python>=3.6.0`
  - `python-build`
  - `python-installer` or `python-pip`
- `python -m build`
- `python -m installer --destdir="$destdir" dist/*.whl` or `pip install dist/*.whl`

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
